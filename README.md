# Anitwt Followers

![UnityChan](./ReadmeVisualContent/AnitwtFollowers.png)

This project was developed using an external project as its base.
The Unity-Chan character was animated in its own project so this one is built on top of that one.
Thanks to the Unity-Chan project we can enjoy not only the character but the animations tha comes with it.

## Goal of the game

![Sunset](./ReadmeVisualContent/Sunset.png)

The main goal of the Anitwt Followers Game is to gain followers. There's small Gummy Bears distributed across the level.
Every time you pick up one, is a new follower and there's 100 of them, each one with a different name.
Once you have them all, you can restart the game and a new set of names would be generated for you to find.

![FollowersGenerator](./ReadmeVisualContent/FollowersListGenerator.png)

The Gommy Bears are all over the level and the further you go, more difficult is to get them.
You need to jump between the platforms and use some of them to move in the level.
Careful not to fall.

![RedBear](./ReadmeVisualContent/WoomyBear.png)